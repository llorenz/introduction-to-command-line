# Profile

Tom Ato is an accomplished wet lab scientist who soon will be taking an intermediate course on genomic data analysis

Lisa is a starting as a new PhD student at the EBI. She has limited experience with using programming languages on her Windows laptop. She has never used the command line before. Now, she needs to access the EBI Codon Cluster and therefore wants to learn basic commands for the command line. After learning about this introductory course she fills confident to run her scripts on a linux-based high performance computing (hpc) cluster.