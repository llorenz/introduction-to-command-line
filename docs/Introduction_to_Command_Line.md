# Introduction to Command Line

## Learning objective
* Introduce fundamental commands of the Linux Command-line to navigate a file system

## Learning outcomes
* recognise a command-line interface
* Use basic shell commands to navigate a file system.
* demonstrate chaining of multiple shell commands
* Use commands to edit files.

https://docs.google.com/presentation/d/1XRm45_zSJw7-2FAFOb_vxJq00BgBa2TxwNZtJ3zuWg4/edit#slide=id.g13b4e02a5db_0_0

learning outcomes "what you actually get"
learning objectives "what the session is trying to achieve"
“Learner profiles”